# 支付宝支付

#### 获取支付宝支付链接
```js
const type = 'mobile'; // 手机端mobile 电脑端pc
const outTradeNo = '123124124125325'; // 商户网站唯一订单号    不可空
const totalFee = '0.01'; // 总金额    不可空
const subject = '一品奶茶'; // 商品名称    不可空
const body = '好喝不贵'; // 商品详情    不可空  
const showUrl = 'http://' + ctx.host; // 商品展示链接
const returnUrl = 'https://www.baidu.com'; // 支付完成跳转链接

const alipayUrl = await module.alipay(type, outTradeNo, totalFee, subject, body, showUrl, returnUrl); // 支付url
```

#### 云函数支付成功回调
```js
const result = await module.alipayNotifyValidate('mobile'); // 回调安全验证
ctx.log(result); // 获取支付宝支付返回的数据
ctx.log(result.outTradeNo); // 获取支付宝支付订单号

if(result && result.tradeStatus === 'TRADE_SUCCESS'){
  // 验证成功 && 支付成功
  // 业务逻辑 ToDo...
}
```