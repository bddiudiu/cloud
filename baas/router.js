const Router = require("koa-router");
const router = new Router();

router.use("", require("./router/auth").routes());
router.use("", require("./router/class").routes());
router.use("", require("./router/debug").routes());
router.use("", require("./router/function").routes());
router.use("", require("./router/image").routes());
router.use("", require("./router/table").routes());

module.exports = router;
